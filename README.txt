Luis Lain


Table of Contents
_________________

1. Introduction
2. Quick Start
3. Description


1 Introduction
==============

  GNU Emacs transient mode for `aws'


2 Quick Start
=============

  Git clone this repository
  ,----
  | cd /usr/local/src
  | git clone https://gitlab.com/lll-tools/emacs/lll-emacs-aws.git
  `----
  Add the `src' directory to the GNU Emacs `load-path' variable.


3 Description
=============

  Manage AWS resources
