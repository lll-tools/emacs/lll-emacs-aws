- [Introduction](#orgd4dcfef)
- [Quick Start](#org4ff78e7)
- [Description](#org6e9f823)


<a id="orgd4dcfef"></a>

# Introduction

GNU Emacs transient mode for `aws`


<a id="org4ff78e7"></a>

# Quick Start

Git clone this repository

```shell
cd /usr/local/src
git clone https://gitlab.com/lll-tools/emacs/lll-emacs-aws.git
```

Add the `src` directory to the GNU Emacs `load-path` variable.


<a id="org6e9f823"></a>

# Description

Manage AWS resources
