;;; lll-aws-s3.el --- s3 file of lll-emacs-aws

;; Copyright (C) 2021 luislain.com

;; Maintainer: lll@luislain.com
;; Keywords: kubernetes
;; Package: lll-emacs-aws
;; Version: 0.1.0
;; Package-Requires: ((emacs "24.5") (transient "0.2.0"))

;; This file is NOT part of GNU Emacs.

;;; Commentary:

;;; Code:

(defvar lll-aws-s3-selected ""
  "AWS s3 selected.")

(defun lll-aws-s3-remove ()
  "Remove aws s3."
  (interactive)
  (let* ((lll-aws-s3-selected-list (tablist-get-marked-items))
	 (lll-aws-s3-selected (car lll-aws-s3-selected-list))
	 (lll-aws-s3-selected-name (car lll-aws-s3-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-aws-s3-remove*" nil
		    lll-aws-command nil
		    ;; TODO
		    ;; (car (split-string lll-aws-arguments))
		    ;; (car (cdr (split-string lll-aws-arguments)))
		    "s3" "rb"
		    (concat "s3://" lll-aws-s3-selected-name)
		    )))
  )

(defun lll-aws-s3-list ()
  "List aws s3."
  (interactive)
  (let* ((lll-aws-s3-selected-list (tablist-get-marked-items))
	 (lll-aws-s3-selected (car lll-aws-s3-selected-list))
	 (lll-aws-s3-selected-name (car lll-aws-s3-selected))
	 )
    (message "Selected: (%s)" lll-aws-s3-selected-name)
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-aws-s3-list*" nil
		    lll-aws-command nil
		    ;; TODO
		    ;; (car (split-string lll-aws-arguments))
		    ;; (car (cdr (split-string lll-aws-arguments)))
		    "s3" "ls"
		    (concat "s3://" lll-aws-s3-selected-name)
		     "--recursive"
		    )))
  )

(defun lll-aws-s3-make ()
  "Make aws s3."
  (interactive)
  (let* ((lll-aws-s3-selected-list (tablist-get-marked-items))
	 (lll-aws-s3-selected (car lll-aws-s3-selected-list))
	 (lll-aws-s3-selected-name (car lll-aws-s3-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-aws-s3-make*" nil
		    lll-aws-command nil
		    ;; TODO
		    ;; (car (split-string lll-aws-arguments))
		    ;; (car (cdr (split-string lll-aws-arguments)))
		    "s3" "mb"
		    (concat "s3://" (read-from-minibuffer "Bucket name: "))
		    )))
  )

;; S3 definition -----------------------------------------------
(define-transient-command lll-aws-s3-help ()
  "Help transient for lll aws s3 mode"
  :man-page "lll-aws-s3-help"
  [:description lll-aws-heading
   ;; ("?" "Describe mode"		describe-mode)
   ("m" "Make s3"	lll-aws-s3-make)
   ("l" "List s3"	lll-aws-s3-list)
   ("r" "Remove s3"	lll-aws-s3-remove)
   ]
  )

(defvar lll-aws-s3-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "?" 'lll-aws-s3-help)
    (define-key map "m" 'lll-aws-s3-make)
    (define-key map "l" 'lll-aws-s3-list)
    (define-key map "r" 'lll-aws-s3-remove)
    map)
  "Keymap for `lll-aws-s3-mode'.")

(defun lll-aws-s3-status-face (status)
  "Return the correct face according to STATUS."
  (cond
   ((s-contains? "Paused" status)
    'lll-aws-face-status-other)
   ((s-starts-with? "Ready" status)
    'lll-aws-face-status-up)
   ((s-starts-with? "Error" status)
    'lll-aws-face-status-down)
   (t
    'default)))

(defun lll-aws-s3-parse (line)
  "Convert a LINE to a `tabulated-list-entries' entry."
  (condition-case nil
      (let* ((lll-s3-entry (vconcat (split-string line " " t)))
      	     (lll-s3-entry-length (length lll-s3-entry))
	     (lll-s3-status (aref lll-s3-entry 2)))
	(aset lll-s3-entry 2
	      (propertize lll-s3-status
			  'font-lock-face (lll-aws-s3-status-face lll-s3-status)))
      	(list (aref lll-s3-entry 2) lll-s3-entry))
    (error "Could not read string:\n%s" line)))

(defun lll-aws-s3-entries ()
  "Return the aws s3s data for `tabulated-list-entries' entry."
  (let* (
	 (command (concat lll-aws-command lll-aws-arguments " s3 ls"))
	 (data (shell-command-to-string command))
	 (lines (split-string data "[\n\r]" t)))
    (-map #'lll-aws-s3-parse lines)))

(defun lll-aws-s3-refresh ()
  "Refresh aws s3 list."
  (setq tabulated-list-entries (lll-aws-s3-entries)))

;; (defcustom lll-aws-s3-default-sort-key '("NAME" . nil)
;;   "Sort key for docker containers.

;; This should be a cons cell (NAME . FLIP) where
;; NAME is a string matching one of the column names
;; and FLIP is a boolean to specify the sort order."
;;   :group 'lll-aws
;;   :type '(cons (choice (const "NAME")
;;                        (const "STATUS")
;;                        (const "ROLES")
;;                        (const "AGE")
;;                        (const "VERSION")
;; 		       )
;;                (choice (const :tag "Ascending" nil)
;;                        (const :tag "Descending" t))))

(define-derived-mode lll-aws-s3-mode tabulated-list-mode
  "LLL aws s3s"
  "Major mode for handling s3s"
  (setq tabulated-list-format [("DATE" 15 t)
			       ("TIME" 15 t)
			       ("BUCKET" 35 t)
			       ])
  (setq tabulated-list-padding 2)
  (add-hook 'tabulated-list-revert-hook 'lll-aws-s3-refresh nil t)
  (tabulated-list-init-header)
  (tablist-minor-mode))

;;;###autoload
(defun lll-aws-s3 ()
  "AWS S3 list."
  (interactive)
  (when (lll-aws-arguments)
    (setq lll-aws-arguments "")
    (let ((arguments (lll-aws-arguments)))
      (while (> (length arguments) 0)
	(setq lll-aws-arguments (concat lll-aws-arguments " " (pop arguments))))
      ))
  (pop-to-buffer "*lll-aws-s3*")
  (lll-aws-s3-mode)
  (tabulated-list-revert))
;; S3 definition -----------------------------------------------

(provide 'lll-aws-s3)

;;; lll-aws-s3.el ends here
