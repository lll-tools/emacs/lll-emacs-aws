;;; lll-aws-iam-roles.el --- iam-roles file of lll-emacs-aws

;; Copyright (C) 2021 luislain.com

;; Maintainer: lll@luislain.com
;; Keywords: kubernetes
;; Package: lll-emacs-aws
;; Version: 0.1.0
;; Package-Requires: ((emacs "24.5") (transient "0.2.0"))

;; This file is NOT part of GNU Emacs.

;;; Commentary:

;;; Code:

(defvar lll-aws-iam-roles-selected ""
  "AWS iam-roles selected.")

(defun lll-aws-iam-roles-remove ()
  "Remove aws iam-roles."
  (interactive)
  (let* ((lll-aws-iam-roles-selected-list (tablist-get-marked-items))
	 (lll-aws-iam-roles-selected (car lll-aws-iam-roles-selected-list))
	 (lll-aws-iam-roles-selected-name (car lll-aws-iam-roles-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-aws-iam-roles-remove*" nil
		    lll-aws-command nil
		    ;; TODO
		    ;; (car (split-string lll-aws-arguments))
		    ;; (car (cdr (split-string lll-aws-arguments)))
		    "iam-roles" "rb"
		    (concat "iam-roles://" lll-aws-iam-roles-selected-name)
		    )))
  )

(defun lll-aws-iam-roles-list ()
  "List aws iam-roles."
  (interactive)
  (let* ((lll-aws-iam-roles-selected-list (tablist-get-marked-items))
	 (lll-aws-iam-roles-selected (car lll-aws-iam-roles-selected-list))
	 (lll-aws-iam-roles-selected-name (car lll-aws-iam-roles-selected))
	 )
    (message "Selected: (%s)" lll-aws-iam-roles-selected-name)
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-aws-iam-roles-list*" nil
		    lll-aws-command nil
		    ;; TODO
		    ;; (car (split-string lll-aws-arguments))
		    ;; (car (cdr (split-string lll-aws-arguments)))
		    "iam-roles" "describe-table"
		    "--table-name"
		    lll-aws-iam-roles-selected-name
		    )))
  )

(defun lll-aws-iam-roles-make ()
  "Make aws iam-roles."
  (interactive)
  (let* ((lll-aws-iam-roles-selected-list (tablist-get-marked-items))
	 (lll-aws-iam-roles-selected (car lll-aws-iam-roles-selected-list))
	 (lll-aws-iam-roles-selected-name (car lll-aws-iam-roles-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-aws-iam-roles-make*" nil
		    lll-aws-command nil
		    ;; TODO
		    ;; (car (split-string lll-aws-arguments))
		    ;; (car (cdr (split-string lll-aws-arguments)))
		    "iam-roles" "mb"
		    (concat "iam-roles://" (read-from-minibuffer "Bucket name: "))
		    )))
  )

;; IAM-ROLES definition -----------------------------------------------
(define-transient-command lll-aws-iam-roles-help ()
  "Help transient for lll aws iam-roles mode"
  :man-page "lll-aws-iam-roles-help"
  [:description lll-aws-heading
   ;; ("?" "Describe mode"		describe-mode)
   ("m" "Make iam-roles"	lll-aws-iam-roles-make)
   ("l" "List iam-roles"	lll-aws-iam-roles-list)
   ("r" "Remove iam-roles"	lll-aws-iam-roles-remove)
   ]
  )

(defvar lll-aws-iam-roles-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "?" 'lll-aws-iam-roles-help)
    (define-key map "m" 'lll-aws-iam-roles-make)
    (define-key map "l" 'lll-aws-iam-roles-list)
    (define-key map "r" 'lll-aws-iam-roles-remove)
    map)
  "Keymap for `lll-aws-iam-roles-mode'.")

(defun lll-aws-iam-roles-status-face (status)
  "Return the correct face according to STATUS."
  (cond
   ((s-contains? "Paused" status)
    'lll-aws-face-status-other)
   ((s-starts-with? "Ready" status)
    'lll-aws-face-status-up)
   ((s-starts-with? "Error" status)
    'lll-aws-face-status-down)
   (t
    'default)))

(defun lll-aws-iam-roles-parse (line)
  "Convert a LINE to a `tabulated-list-entries' entry."
  (condition-case nil
      (let* (
	     (lll-iam-roles-entry (vconcat line))
      	     (lll-iam-roles-entry-length (length lll-iam-roles-entry))
	     (lll-iam-roles-entry-name (alist-get 'RoleName line))
	     (lll-iam-roles-entry-path (alist-get 'Path line))
	     (lll-iam-roles-entry-id (alist-get 'RoleId line))
	     (lll-iam-roles-entry-arn (alist-get 'Arn line))
	     (lll-iam-roles-entry-date (alist-get 'CreateDate line))
	     ;; (lll-iam-roles-entry-max (alist-get 'MaxSessionDuration line))
	     ;; (lll-iam-roles-entry-policy (alist-get 'AssumeRolePolicyDocument line))
	     )
	;; (aset lll-iam-roles-entry 2
	;;       (propertize lll-iam-roles-status
	;; 		  'font-lock-face (lll-aws-iam-roles-status-face lll-iam-roles-status)))
      	(list lll-iam-roles-entry-name (vector lll-iam-roles-entry-date
					       lll-iam-roles-entry-id
					       lll-iam-roles-entry-name
					       lll-iam-roles-entry-path
					       lll-iam-roles-entry-arn
					       ;; lll-iam-roles-entry-max
					       )))
    (error "Could not read string:\n%s" line))
  )

(defun lll-aws-iam-roles-entries ()
  "Return the aws iam-roless data for `tabulated-list-entries' entry."
  (let* (
	 (command (concat lll-aws-command lll-aws-arguments " iam list-roles"))
	 (data (shell-command-to-string command))
	 (datajson (condition-case nil
		       (json-read-from-string data)
		     (error nil)))
	 (lines (alist-get 'Roles datajson))
	 )
    (-map #'lll-aws-iam-roles-parse lines))
  )

(defun lll-aws-iam-roles-refresh ()
  "Refresh aws iam-roles list."
  (setq tabulated-list-entries (lll-aws-iam-roles-entries)))

(define-derived-mode lll-aws-iam-roles-mode tabulated-list-mode
  "LLL aws iam-roless"
  "Major mode for handling iam-roless"
  (setq tabulated-list-format [
			       ("CreateDate" 25 t)
			       ("RoleId" 21 t)
			       ("RoleName" 40 t)
			       ("Path" 30 t)
			       ("Arn" 40 t)
			       ;; ("MaxSessionDuration" 35 t)
			       ])
  (setq tabulated-list-padding 2)
  (add-hook 'tabulated-list-revert-hook 'lll-aws-iam-roles-refresh nil t)
  (tabulated-list-init-header)
  (tablist-minor-mode))

;;;###autoload
(defun lll-aws-iam-roles ()
  "AWS IAM-ROLES list."
  (interactive)
  (when (lll-aws-arguments)
    (setq lll-aws-arguments "")
    (let ((arguments (lll-aws-arguments)))
      (while (> (length arguments) 0)
	(setq lll-aws-arguments (concat lll-aws-arguments " " (pop arguments))))
      ))
  (pop-to-buffer "*lll-aws-iam-roles*")
  (lll-aws-iam-roles-mode)
  (tabulated-list-revert))
;; IAM-ROLES definition -----------------------------------------------

(provide 'lll-aws-iam-roles)

;;; lll-aws-iam-roles.el ends here
