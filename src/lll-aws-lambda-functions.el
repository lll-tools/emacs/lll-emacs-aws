;;; lll-aws-lambda-functions.el --- lambda-functions file of lll-emacs-aws

;; Copyright (C) 2021 luislain.com

;; Maintainer: lll@luislain.com
;; Keywords: kubernetes
;; Package: lll-emacs-aws
;; Version: 0.1.0
;; Package-Requires: ((emacs "24.5") (transient "0.2.0"))

;; This file is NOT part of GNU Emacs.

;;; Commentary:

;;; Code:

(defvar lll-aws-lambda-functions-selected ""
  "AWS lambda-functions selected.")

(defun lll-aws-lambda-functions-remove ()
  "Remove aws lambda-functions."
  (interactive)
  (let* ((lll-aws-lambda-functions-selected-list (tablist-get-marked-items))
	 (lll-aws-lambda-functions-selected (car lll-aws-lambda-functions-selected-list))
	 (lll-aws-lambda-functions-selected-name (car lll-aws-lambda-functions-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-aws-lambda-functions-remove*" nil
		    lll-aws-command nil
		    ;; TODO
		    ;; (car (split-string lll-aws-arguments))
		    ;; (car (cdr (split-string lll-aws-arguments)))
		    "lambda-functions" "rb"
		    (concat "lambda-functions://" lll-aws-lambda-functions-selected-name)
		    )))
  )

(defun lll-aws-lambda-functions-list ()
  "List aws lambda-functions."
  (interactive)
  (let* ((lll-aws-lambda-functions-selected-list (tablist-get-marked-items))
	 (lll-aws-lambda-functions-selected (car lll-aws-lambda-functions-selected-list))
	 (lll-aws-lambda-functions-selected-name (car lll-aws-lambda-functions-selected))
	 )
    (message "Selected: (%s)" lll-aws-lambda-functions-selected-name)
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-aws-lambda-functions-list*" nil
		    lll-aws-command nil
		    ;; TODO
		    ;; (car (split-string lll-aws-arguments))
		    ;; (car (cdr (split-string lll-aws-arguments)))
		    "lambda-functions" "describe-table"
		    "--table-name"
		    lll-aws-lambda-functions-selected-name
		    )))
  )

(defun lll-aws-lambda-functions-make ()
  "Make aws lambda-functions."
  (interactive)
  (let* ((lll-aws-lambda-functions-selected-list (tablist-get-marked-items))
	 (lll-aws-lambda-functions-selected (car lll-aws-lambda-functions-selected-list))
	 (lll-aws-lambda-functions-selected-name (car lll-aws-lambda-functions-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-aws-lambda-functions-make*" nil
		    lll-aws-command nil
		    ;; TODO
		    ;; (car (split-string lll-aws-arguments))
		    ;; (car (cdr (split-string lll-aws-arguments)))
		    "lambda-functions" "mb"
		    (concat "lambda-functions://" (read-from-minibuffer "Bucket name: "))
		    )))
  )

;; LAMBDA-FUNCTIONS definition -----------------------------------------------
(define-transient-command lll-aws-lambda-functions-help ()
  "Help transient for lll aws lambda-functions mode"
  :man-page "lll-aws-lambda-functions-help"
  [:description lll-aws-heading
   ;; ("?" "Describe mode"		describe-mode)
   ("m" "Make lambda-functions"	lll-aws-lambda-functions-make)
   ("l" "List lambda-functions"	lll-aws-lambda-functions-list)
   ("r" "Remove lambda-functions"	lll-aws-lambda-functions-remove)
   ]
  )

(defvar lll-aws-lambda-functions-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "?" 'lll-aws-lambda-functions-help)
    (define-key map "m" 'lll-aws-lambda-functions-make)
    (define-key map "l" 'lll-aws-lambda-functions-list)
    (define-key map "r" 'lll-aws-lambda-functions-remove)
    map)
  "Keymap for `lll-aws-lambda-functions-mode'.")

(defun lll-aws-lambda-functions-status-face (status)
  "Return the correct face according to STATUS."
  (cond
   ((s-contains? "Paused" status)
    'lll-aws-face-status-other)
   ((s-starts-with? "Ready" status)
    'lll-aws-face-status-up)
   ((s-starts-with? "Error" status)
    'lll-aws-face-status-down)
   (t
    'default)))

(defun lll-aws-lambda-functions-parse (line)
  "Convert a LINE to a `tabulated-list-entries' entry."
  (condition-case nil
      (let* (
	     (lll-lambda-functions-entry (vconcat line))
      	     (lll-lambda-functions-entry-length (length lll-lambda-functions-entry))
	     (lll-lambda-functions-entry-name (alist-get 'FunctionName line))
	     (lll-lambda-functions-entry-arn (alist-get 'FunctionArn line))
	     (lll-lambda-functions-entry-runtime (alist-get 'Runtime line))
	     (lll-lambda-functions-entry-handler (alist-get 'Handler line))
	     ;; (lll-lambda-functions-entry-mem (alist-get 'MemorySize line))
	     (lll-lambda-functions-entry-date (alist-get 'LastModified line))
	     (lll-lambda-functions-entry-id (alist-get 'RevisionId line))
	     (lll-lambda-functions-entry-type (alist-get 'PackageType line))
	     )
      	(list lll-lambda-functions-entry-name (vector lll-lambda-functions-entry-date
					       lll-lambda-functions-entry-id
					       lll-lambda-functions-entry-name
					       lll-lambda-functions-entry-handler
					       lll-lambda-functions-entry-runtime
					       ;; lll-lambda-functions-entry-mem
					       lll-lambda-functions-entry-type
					       lll-lambda-functions-entry-arn
					       )))
    (error "Could not read string:\n%s" line))
  )

(defun lll-aws-lambda-functions-entries ()
  "Return the aws lambda-functionss data for `tabulated-list-entries' entry."
  (let* (
	 (command (concat lll-aws-command lll-aws-arguments " lambda list-functions"))
	 (data (shell-command-to-string command))
	 (datajson (condition-case nil
		       (json-read-from-string data)
		     (error nil)))
	 (lines (alist-get 'Functions datajson))
	 )
    (-map #'lll-aws-lambda-functions-parse lines))
  )

(defun lll-aws-lambda-functions-refresh ()
  "Refresh aws lambda-functions list."
  (setq tabulated-list-entries (lll-aws-lambda-functions-entries)))

(define-derived-mode lll-aws-lambda-functions-mode tabulated-list-mode
  "LLL aws lambda-functionss"
  "Major mode for handling lambda-functionss"
  (setq tabulated-list-format [
			       ("LastModified" 35 t)
			       ("RevisionId" 35 t)
			       ("FunctionName" 40 t)
			       ("Handler" 20 t)
			       ("Runtime" 20 t)
			       ;; ("MemorySize" 30 t)
			       ("PackageType" 10 t)
			       ("FunctionArn" 40 t)
			       ])
  (setq tabulated-list-padding 2)
  (add-hook 'tabulated-list-revert-hook 'lll-aws-lambda-functions-refresh nil t)
  (tabulated-list-init-header)
  (tablist-minor-mode))

;;;###autoload
(defun lll-aws-lambda-functions ()
  "AWS LAMBDA-FUNCTIONS list."
  (interactive)
  (when (lll-aws-arguments)
    (setq lll-aws-arguments "")
    (let ((arguments (lll-aws-arguments)))
      (while (> (length arguments) 0)
	(setq lll-aws-arguments (concat lll-aws-arguments " " (pop arguments))))
      ))
  (pop-to-buffer "*lll-aws-lambda-functions*")
  (lll-aws-lambda-functions-mode)
  (tabulated-list-revert))
;; LAMBDA-FUNCTIONS definition -----------------------------------------------

(provide 'lll-aws-lambda-functions)

;;; lll-aws-lambda-functions.el ends here
