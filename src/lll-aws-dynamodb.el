;;; lll-aws-dynamodb.el --- dynamodb file of lll-emacs-aws

;; Copyright (C) 2021 luislain.com

;; Maintainer: lll@luislain.com
;; Keywords: kubernetes
;; Package: lll-emacs-aws
;; Version: 0.1.0
;; Package-Requires: ((emacs "24.5") (transient "0.2.0"))

;; This file is NOT part of GNU Emacs.

;;; Commentary:

;;; Code:

(defvar lll-aws-dynamodb-selected ""
  "AWS dynamodb selected.")

(defun lll-aws-dynamodb-remove ()
  "Remove aws dynamodb."
  (interactive)
  (let* ((lll-aws-dynamodb-selected-list (tablist-get-marked-items))
	 (lll-aws-dynamodb-selected (car lll-aws-dynamodb-selected-list))
	 (lll-aws-dynamodb-selected-name (car lll-aws-dynamodb-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-aws-dynamodb-remove*" nil
		    lll-aws-command nil
		    ;; TODO
		    ;; (car (split-string lll-aws-arguments))
		    ;; (car (cdr (split-string lll-aws-arguments)))
		    "dynamodb" "rb"
		    (concat "dynamodb://" lll-aws-dynamodb-selected-name)
		    )))
  )

(defun lll-aws-dynamodb-list ()
  "List aws dynamodb."
  (interactive)
  (let* ((lll-aws-dynamodb-selected-list (tablist-get-marked-items))
	 (lll-aws-dynamodb-selected (car lll-aws-dynamodb-selected-list))
	 (lll-aws-dynamodb-selected-name (car lll-aws-dynamodb-selected))
	 )
    (message "Selected: (%s)" lll-aws-dynamodb-selected-name)
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-aws-dynamodb-list*" nil
		    lll-aws-command nil
		    ;; TODO
		    ;; (car (split-string lll-aws-arguments))
		    ;; (car (cdr (split-string lll-aws-arguments)))
		    "dynamodb" "describe-table"
		    "--table-name"
		    lll-aws-dynamodb-selected-name
		    )))
  )

(defun lll-aws-dynamodb-make ()
  "Make aws dynamodb."
  (interactive)
  (let* ((lll-aws-dynamodb-selected-list (tablist-get-marked-items))
	 (lll-aws-dynamodb-selected (car lll-aws-dynamodb-selected-list))
	 (lll-aws-dynamodb-selected-name (car lll-aws-dynamodb-selected))
	 )
    (pop-to-buffer (make-comint-in-buffer
		    "*lll-aws-dynamodb-make*" nil
		    lll-aws-command nil
		    ;; TODO
		    ;; (car (split-string lll-aws-arguments))
		    ;; (car (cdr (split-string lll-aws-arguments)))
		    "dynamodb" "mb"
		    (concat "dynamodb://" (read-from-minibuffer "Bucket name: "))
		    )))
  )

;; DYNAMODB definition -----------------------------------------------
(define-transient-command lll-aws-dynamodb-help ()
  "Help transient for lll aws dynamodb mode"
  :man-page "lll-aws-dynamodb-help"
  [:description lll-aws-heading
   ;; ("?" "Describe mode"		describe-mode)
   ("m" "Make dynamodb"	lll-aws-dynamodb-make)
   ("l" "List dynamodb"	lll-aws-dynamodb-list)
   ("r" "Remove dynamodb"	lll-aws-dynamodb-remove)
   ]
  )

(defvar lll-aws-dynamodb-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "?" 'lll-aws-dynamodb-help)
    (define-key map "m" 'lll-aws-dynamodb-make)
    (define-key map "l" 'lll-aws-dynamodb-list)
    (define-key map "r" 'lll-aws-dynamodb-remove)
    map)
  "Keymap for `lll-aws-dynamodb-mode'.")

(defun lll-aws-dynamodb-status-face (status)
  "Return the correct face according to STATUS."
  (cond
   ((s-contains? "Paused" status)
    'lll-aws-face-status-other)
   ((s-starts-with? "Ready" status)
    'lll-aws-face-status-up)
   ((s-starts-with? "Error" status)
    'lll-aws-face-status-down)
   (t
    'default)))

(defun lll-aws-dynamodb-parse (line)
  "Convert a LINE to a `tabulated-list-entries' entry."
  (condition-case nil
      (let* ((lll-dynamodb-entry (vconcat (split-string line " " t)))
      	     (lll-dynamodb-entry-length (length lll-dynamodb-entry))
	     ;; (lll-dynamodb-status (aref lll-dynamodb-entry 2))
	     )
	;; (aset lll-dynamodb-entry 2
	;;       (propertize lll-dynamodb-status
	;; 		  'font-lock-face (lll-aws-dynamodb-status-face lll-dynamodb-status)))
      	(list (aref lll-dynamodb-entry 0) lll-dynamodb-entry))
    (error "Could not read string:\n%s" line))
  )

(defun lll-aws-dynamodb-entries ()
  "Return the aws dynamodbs data for `tabulated-list-entries' entry."
  (let* (
	 (command (concat lll-aws-command lll-aws-arguments " dynamodb list-tables"))
	 (data (shell-command-to-string command))
	 (datajson (condition-case nil
		       (json-read-from-string data)
		     (error nil)))
	 (lines (alist-get 'TableNames datajson))
	 )
    (-map #'lll-aws-dynamodb-parse lines))
  )

(defun lll-aws-dynamodb-refresh ()
  "Refresh aws dynamodb list."
  (setq tabulated-list-entries (lll-aws-dynamodb-entries)))

(define-derived-mode lll-aws-dynamodb-mode tabulated-list-mode
  "LLL aws dynamodbs"
  "Major mode for handling dynamodbs"
  (setq tabulated-list-format [
			       ("TABLE" 35 t)
			       ])
  (setq tabulated-list-padding 2)
  (add-hook 'tabulated-list-revert-hook 'lll-aws-dynamodb-refresh nil t)
  (tabulated-list-init-header)
  (tablist-minor-mode))

;;;###autoload
(defun lll-aws-dynamodb ()
  "AWS DYNAMODB list."
  (interactive)
  (when (lll-aws-arguments)
    (setq lll-aws-arguments "")
    (let ((arguments (lll-aws-arguments)))
      (while (> (length arguments) 0)
	(setq lll-aws-arguments (concat lll-aws-arguments " " (pop arguments))))
      ))
  (pop-to-buffer "*lll-aws-dynamodb*")
  (lll-aws-dynamodb-mode)
  (tabulated-list-revert))
;; DYNAMODB definition -----------------------------------------------

(provide 'lll-aws-dynamodb)

;;; lll-aws-dynamodb.el ends here
