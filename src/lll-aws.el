;;; lll-aws.el --- main file of lll-emacs-aws

;; Copyright (C) 2021 luislain.com

;; Maintainer: lll@luislain.com
;; Keywords: kubernetes
;; Package: lll-emacs-aws
;; Version: 0.1.0
;; Package-Requires: ((emacs "24.5") (transient "0.2.0"))

;; This file is NOT part of GNU Emacs.

;;; Commentary:

;;; Code:

(require 'transient)
(require 'dash)
(require 'json)
;; https://github.com/magnars/s.el
;; (require 's)

(require 'lll-aws-config)
;; (require 'lll-aws-allns)
(require 'lll-aws-s3)
;; (require 'lll-aws-secret)
(require 'lll-aws-iam-roles)
(require 'lll-aws-lambda-functions)
(require 'lll-aws-dynamodb)
;; (require 'lll-aws-deployment)
;; (require 'lll-aws-pv)
;; (require 'lll-aws-pvc)
;; (require 'lll-aws-rs)
;; (require 'lll-aws-ds)
;; (require 'lll-aws-sts)
;; (require 'lll-aws-svc)
;; (require 'lll-aws-ing)
;; (require 'lll-aws-hpa)

;; configmap
;; endpoints

(defgroup lll-aws nil
  "LLL aws customization group"
  :group 'convenience)

(defcustom lll-aws-command "aws"
  "LLL aws command."
  :group 'lll-aws
  :type 'string)

(defcustom lll-aws-config "$HOME/.aws/config"
  "Configfile."
  :group 'lll-aws
  :type 'string)

(defcustom lll-aws-arguments ""
  "Arguments for aws."
  :group 'lll-aws
  :type 'string)

(defface lll-aws-face-status-up
  '((t :inherit success))
  "Face used when the status is up."
  :group 'lll-aws)

(defface lll-aws-face-status-down
  '((t :inherit error))
  "Face used when the status is down"
  :group 'lll-aws)

(defface lll-aws-face-status-other
  '((t :inherit warning))
  "Face used when the status is not up/down."
  :group 'lll-aws)

(defun lll-aws-arguments ()
  "Return the latest used arguments in the `lll-aws' transient."
  (car (alist-get 'lll-aws transient-history)))

(defun lll-aws-run-command (&rest args)
  "Execute \"`lll-aws-command' ARGS\" and return the results."
  (let* ((flat-args (-remove 's-blank? (-flatten (list (lll-aws-arguments) args))))
         (command (s-join " " (-insert-at 0 lll-aws-command flat-args))))
    (message command)
    (s-trim-right (shell-command-to-string command))))

;; (defun lll-aws-age2hours (age)
;;   "Return the AGE in hours."
;;   ;; (setq age "1d1h")
;;   (if (not (string-match-p "d" age))
;;       (and (setq lll-aws-age-days "0")
;; 	   (setq lll-aws-age-hours-text age))
;;     (setq lll-aws-age-list (split-string age "d"))
;;     (setq lll-aws-age-days (pop lll-aws-age-list))
;;     (setq lll-aws-age-hours-text (pop lll-aws-age-list)))

;;   (if (not (string-match-p "h" lll-aws-age-hours-text))
;;       (and (setq lll-aws-age-hours "0")
;; 	   (setq lll-aws-age-minutes-text age))
;;     (setq lll-aws-age-list (split-string age "h"))
;;     (setq lll-aws-age-hours (pop lll-aws-age-list))
;;     (setq lll-aws-age-minutes-text (pop lll-aws-age-list)))

;;   ;; (message (format "%s days ; %s hours"
;;   ;; 		   lll-aws-age-days
;;   ;; 		   lll-aws-age-hours))
;;   (format "%06dh-(%s)"
;; 	  (+ (* (string-to-number lll-aws-age-days) 24)
;; 	     (string-to-number lll-aws-age-hours))
;; 	  age)
;;   )

(defun lll-aws-get-marked-items-ids ()
  "Get the marked items from `tablist-get-marked-items'."
  (-map #'car (tablist-get-marked-items)))

;; (defun lll-aws-heading-actions ()
;;   "Heading actions."
;;   (let ((items (s-join ", " (lll-aws-get-marked-items-ids))))
;;     (format "%s (%s) %s (%s) %s %s"
;;             (propertize "Profile" 'face 'transient-heading)
;;             (propertize lll-aws-config-profile 'face 'transient-value)
;;             (propertize "Arguments" 'face 'transient-heading)
;;             (propertize lll-aws-arguments 'face 'transient-value)
;;             (propertize "Actions on" 'face 'transient-heading)
;;             (propertize items        'face 'transient-value)
;; 	    )))

(defun lll-aws-heading ()
  "Show heading info."
  ;;(setq lll-aws-config-profile (getenv "AWS_PROFILE"))
  ;;(lll-aws-config-stsinfo)
  (format "%s (%s) %s (%s) %s (%s) %s (%s)"
	  (propertize "LLL AWS: Profile" 'face 'transient-heading)
	  (propertize lll-aws-config-profile 'face 'transient-value)
	  (propertize "UserId" 'face 'transient-heading)
	  (propertize lll-aws-config-userid 'face 'transient-value)
	  (propertize "Account" 'face 'transient-heading)
	  (propertize lll-aws-config-account 'face 'transient-value)
	  (propertize "Arguments" 'face 'font-lock-keyword-face)
	  (propertize lll-aws-arguments 'face 'font-lock-string-face))
)

(defun lll-aws-read-configfile (prompt &optional initial-input _history)
  "Wrapper PROMPT and INITIAL-INPUT around `read-file-name'."
  (read-file-name prompt nil nil t initial-input (apply-partially 'string-match "config.*")))

;;;###autoload (autoload 'lll-aws "lll-aws" nil t)
(define-transient-command lll-aws ()
  "Transient for aws."
  :man-page "lll-aws"
  ["Arguments"
   ("-C"	"Config file"	"--configfile="		lll-aws-read-configfile)
   ("-R"	"Region"       	"--region="		read-string)
   ;; ("-T"	"Template"     	"--template="		read-string)
   ;; ("-N"	"Namespace"	"-n "			read-string)
   ]
  [:description lll-aws-heading
		("s" "S3 Buckets"			lll-aws-s3)
		("r" "IAM Roles"		lll-aws-iam-roles)
		("f" "Lambda functions"		lll-aws-lambda-functions)
		("d" "DynamoDB Tables"		lll-aws-dynamodb)
   ;; ("d" "Deployments"		lll-aws-deployment)
   ;; ("e" "Services"	   	lll-aws-svc)
   ;; ("s" "Secrets"      		lll-aws-secret)
   ;; ("v" "Persistent Volumes"   	lll-aws-pv)
   ;; ("l" "P. Volume Claims"   	lll-aws-pvc)
   ;; ("r" "ReplicaSets"   	lll-aws-rs)
   ;; ("m" "DaemonSets"	   	lll-aws-ds)
   ;; ("f" "StatefulSets"	   	lll-aws-sts)
   ;; ("i" "Ingress"	   	lll-aws-ing)
   ;; ("h" "H. Pod Autoscalers"	lll-aws-hpa)
   ]
  ["Other"
   ("C" "AWS Config"		lll-aws-config)
   ;; ("A" "ALL Nampespaces"	lll-aws-allns)
   ])

(provide 'lll-aws)

;;; lll-aws.el ends here
