;;; lll-aws-config.el --- config file of lll-emacs-aws

;; Copyright (C) 2021 luislain.com

;; Maintainer: lll@luislain.com
;; Keywords: kubernetes
;; Package: lll-emacs-aws
;; Version: 0.1.0
;; Package-Requires: ((emacs "24.5") (transient "0.2.0"))

;; This file is NOT part of GNU Emacs.

;;; Commentary:
;; $ export AWS_ACCESS_KEY_ID=AKIAIOSFODNN7EXAMPLE
;; $ export AWS_SECRET_ACCESS_KEY=wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
;; $ export AWS_DEFAULT_REGION=us-west-2

;;; Code:

(defvar lll-aws-config-profile ""
  "Selected profile of aws config.")

(defvar lll-aws-config-userid ""
  "AWS UserId of selected profile.")

(defvar lll-aws-config-account ""
  "AWS account of selected profile.")

(defvar lll-aws-config-arn ""
  "AWS ARN of selected profile.")

;; (defun lll-aws-config-apiv ()
;;   "List aws API versions"
;;   (interactive)
;;   ;; (--each (lll-aws-get-marked-items-ids)
;;   ;;   (lll-aws-run-command "api-versions" it))
;;   (pop-to-buffer (make-comint-in-buffer
;; 		  "lll-aws-config-apiv" nil
;; 		  ;; (concat "lll-aws-config-current-" lll-aws-config-selected-name) nil
;; 		  lll-aws-command nil
;; 		  ;;  lll-aws-config-selected-name
;; 		  "api-versions"
;; 		  ))
;;   )

;; (defun lll-aws-config-apir ()
;;   "List aws API resources"
;;   (interactive)
;;   (pop-to-buffer (make-comint-in-buffer
;; 		  "lll-aws-config-apir" nil
;; 		  ;; (concat "lll-aws-config-current-" lll-aws-config-selected-name) nil
;; 		  lll-aws-command nil
;; 		  ;;  lll-aws-config-selected-name
;; 		  "api-resources"
;; 		  ))
;;   )

;; (defun lll-aws-config-info ()
;;   "Cluster info aws"
;;   (interactive)
;;   (pop-to-buffer (make-comint-in-buffer
;; 		  "lll-aws-config-info" nil
;; 		  ;; (concat "lll-aws-config-current-" lll-aws-config-selected-name) nil
;; 		  lll-aws-command nil
;; 		  ;;  lll-aws-config-selected-name
;; 		  "cluster-info"
;; 		  ))
;;   )

;; (defun lll-aws-config-info-dump ()
;;   "Cluster info DUMP aws"
;;   (interactive)
;;   (pop-to-buffer (make-comint-in-buffer
;; 		  "lll-aws-config-info" nil
;; 		  ;; (concat "lll-aws-config-current-" lll-aws-config-selected-name) nil
;; 		  lll-aws-command nil
;; 		  ;;  lll-aws-config-selected-name
;; 		  "cluster-info" "dump"
;; 		  ))
;;   )

(defun lll-aws-config-edit ()
  "Edit AWS Config profile."
  (interactive)
  (setq lll-aws-config-new-profile-name (completing-read "Profile: "
					 (split-string
					  (shell-command-to-string
					   (concat lll-aws-command " configure list-profiles")))
					 nil 'confirm))
  ;; (read-from-minibuffer "Profile name: ")
  (pop-to-buffer (make-comint-in-buffer
		  "*lll-aws-config-profile*" nil
		  lll-aws-command nil
		  "configure" "--profile" lll-aws-config-new-profile-name
		  ))
  )

(defun lll-aws-config-profile ()
  "Set AWS Config profile."
  (interactive)
  (setq lll-aws-config-profile
	(setenv "AWS_PROFILE" (completing-read "Profile: "
					       (split-string
						(shell-command-to-string
						 (concat lll-aws-command " configure list-profiles")))
					       nil t))
	)
  (lll-aws-config-stsinfo)
  (revert-buffer)
  )

(defun lll-aws-config-credentials ()
  "Set AWS Config credentials."
  (interactive)
  (setenv "AWS_PROFILE" "default")
  (pop-to-buffer (make-comint-in-buffer
		  "*lll-aws-config-profile*" nil
		  lll-aws-command nil
		  "configure"
		  ))
  )

(defun lll-aws-config-stsinfo ()
  "AWS info."
  (let ((data (condition-case nil
		  (json-read-from-string
		   (shell-command-to-string
		    (concat lll-aws-command " sts get-caller-identity")))
		(error nil))))
    (if data
	(progn
	  (message "Data: (%s)" data)
	  (setq lll-aws-config-userid (alist-get 'UserId data))
	  (setq lll-aws-config-account (alist-get 'Account data))
	  (setq lll-aws-config-arn (alist-get 'Arn data)))
      (setq lll-aws-config-userid	"")
      (setq lll-aws-config-account	"")
      (setq lll-aws-config-arn		"")
      )
  ))

;; CONFIG definition -----------------------------------------------
(define-transient-command lll-aws-config-help ()
  "Help transient for lll aws config mode."
  [:description lll-aws-heading
   ;; ("?" "Describe mode"		describe-mode)
   ("e" "Edit/create profile"		lll-aws-config-edit)
   ("p" "Set current profile"		lll-aws-config-profile)
   ("c" "Define credentials"		lll-aws-config-credentials)
   ;; ("d" "Current cluster info dump"	lll-aws-config-info-dump)
   ;; ("v" "List api versions"		lll-aws-config-apiv)
   ;; ("r" "List api resources"		lll-aws-config-apir)
   ])

(defvar lll-aws-config-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "?" 'lll-aws-config-help)
    (define-key map "e" 'lll-aws-config-edit)
    (define-key map "p" 'lll-aws-config-profile)
    (define-key map "c" 'lll-aws-config-credentials)
    ;; (define-key map "d" 'lll-aws-config-info-dump)
    ;; (define-key map "v" 'lll-aws-config-apiv)
    ;; (define-key map "r" 'lll-aws-config-apir)
    map)
  "Keymap for `lll-aws-config-mode'.")

(defun lll-aws-config-parse (line)
  "Convert a LINE to a `tabulated-list-entries' entry."
  (condition-case nil
      (let* ((lll-config-list (split-string
				  (replace-regexp-in-string ", " "," line)))
	     (lll-config-list-length (length lll-config-list)))
	(while (< lll-config-list-length 4)
	  (add-to-list 'lll-config-list '"[-]" t)
	  (setq lll-config-list-length (length lll-config-list)))
	(setq lll-config-list-vector (vconcat lll-config-list))
	(list (aref lll-config-list-vector 1) lll-config-list-vector))
    (error "Could not read string:\n%s" line)))

(defun lll-aws-config-entries ()
  "Return the aws configs data for `tabulated-list-entries' entry."
  (let* ((command (concat lll-aws-command " configure list"))
	 (data (shell-command-to-string command))
	 (lines (cdr (cdr (split-string data "[\n\r]" t)))))
    (-map #'lll-aws-config-parse lines)))

(defun lll-aws-config-refresh ()
  "Refresh AWS Config list."
  (setq tabulated-list-entries (lll-aws-config-entries)))

(define-derived-mode lll-aws-config-mode tabulated-list-mode
  "LLL AWS Config"
  "Major mode for handling configs"
  :group 'lll-aws
  (setq tabulated-list-format [
			       ("NAME"		15 nil)
			       ("VALUE" 	25 nil)
			       ("TYPE"		25 nil)
			       ("LOCATION"	30 nil)
			       ])
  (setq tabulated-list-padding 2)
  (add-hook 'tabulated-list-revert-hook 'lll-aws-config-refresh nil t)
  (tabulated-list-init-header)
  (tablist-minor-mode))

;;;###autoload
(defun lll-aws-config ()
  "AWS Config list."
  (interactive)
  (pop-to-buffer "*lll-aws-config*")
  (lll-aws-config-stsinfo)
  (lll-aws-config-mode)
  (tabulated-list-revert)
  )
;; CONFIG definition -----------------------------------------------

(provide 'lll-aws-config)

;;; lll-aws-config.el ends here
